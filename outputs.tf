# module ecs-app outputs

output "task_definition" {
  value = aws_ecs_task_definition.task_definition
}

output "service" {
  value = aws_ecs_service.service
}

output "discovery_service" {
  value = aws_service_discovery_service.discovery_service
}

output "autoscaling_target" {
  value = aws_appautoscaling_target.target
}

output "autoscaling_policy" {
  value = aws_appautoscaling_policy.policy
}
