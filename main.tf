# module ecs-app /main.tf

resource "aws_ecs_task_definition" "task_definition" {
  family                   = var.app.family
  network_mode             = "awsvpc"
  execution_role_arn       = var.app.ecs_task_execution_role.arn
  task_role_arn            = var.app.ecs_task_role.arn
  cpu                      = var.app.cpu
  memory                   = var.app.memory
  requires_compatibilities = ["FARGATE"]
  container_definitions    = var.app.container_definitions

  tags = {
    Environment = var.environment
    ManagedBy  = var.managed_by
  }
}

resource "aws_ecs_service" "service" {
  name            = "${var.app.name}-${var.environment}-ecs-service"
  cluster         = var.app.cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = var.app.desired_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = var.app.security_groups
    subnets          = var.app.subnet_ids
    assign_public_ip = var.app.assign_public_ip
  }

  dynamic "load_balancer" {
    for_each = var.app.use_load_balancer ? ["this"] : []
    content {
      target_group_arn = var.app.lb_target_group_arn
      container_name   = var.app.lb_container_name
      container_port   = var.app.lb_container_port
    }
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  service_registries {
    registry_arn = aws_service_discovery_service.discovery_service.arn
  }

  tags = {
    Environment = var.environment
    ManagedBy  = var.managed_by
  }
}

resource "aws_service_discovery_service" "discovery_service" {
  name     = var.app.discovery_service.name

  dns_config {
    namespace_id = var.app.discovery_service.namespace_id

    dns_records {
      ttl  = var.app.discovery_service.ttl
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = var.app.discovery_service.failure_threshold
  }

  tags = {
    Environment = var.environment
    ManagedBy  = var.managed_by
  }
}

resource "aws_appautoscaling_target" "target" {
  max_capacity       = var.app.autoscaling.max_capacity
  min_capacity       = var.app.autoscaling.min_capacity
  resource_id        = "service/${var.app.cluster.cluster_name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "policy" {
  for_each           = var.app.autoscaling_policies
  name               = each.value.name
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.target.resource_id
  scalable_dimension = aws_appautoscaling_target.target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = each.value.predefined_metric_type
    }

    target_value = each.value.target_value
  }
}
